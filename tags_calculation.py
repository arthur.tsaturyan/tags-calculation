"""
Task Description

Implement a small program in Python that is capable of scanning a web page at a
given URL and retrieve the following information:

1. Number of HTML form elements (with method="get") present in that page
2. Number of HTML image tags present in that page

The program should accept a URL parameter from the command line and do some
basic validation to avoid the input of invalid values.
"""

from lxml import html
import requests
import validators

input_url = str(input())


def tags_calculation(input_url_param):
    url_validation = validators.url(input_url_param)

    if not url_validation:
        return print('Invalid URL')

    web_page_source = requests.get(input_url_param)

    if web_page_source.status_code == 404:
        return print('HTTP 404 - Page not found')

    source = html.fromstring(web_page_source.content)

    all_tags = source.xpath('//*')
    img_tags = source.xpath('//img')

    count_of_all_tags = len(all_tags)
    count_of_img_tags = len(img_tags)

    print('Number of HTML form elements - ' + str(count_of_all_tags))
    print('Number of HTML image tags - ' + str(count_of_img_tags))


tags_calculation(input_url)
